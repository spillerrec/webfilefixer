/*
	This file is part of WebFileFixer.

	WebFileFixer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WebFileFixer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WebFileFixer.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ui_mainwindow.h"
#include "mainwindow.h"

#include <vector>

#include <QFileInfo>
#include <QFile>
#include <QUrl>
#include <QDragEnterEvent>
#include <QDropEvent>

main_widget::main_widget(): QMainWindow(), ui(new Ui_main_widget), org_name_model( &org_names ), name_model( &names ){
	ui->setupUi(this);
	connect( ui->btn_rename, SIGNAL( clicked() ), this, SLOT( rename_files() ) );
	connect( ui->btn_clear, SIGNAL( clicked() ), this, SLOT( clear() ) );
	connect( ui->cbx_percent, SIGNAL( toggled(bool) ), this, SLOT( update_names() ) );
	connect( ui->cbx_underscore, SIGNAL( toggled(bool) ), this, SLOT( update_names() ) );
	connect( ui->cbx_plus, SIGNAL( toggled(bool) ), this, SLOT( update_names() ) );
	connect( ui->cbx_spaces, SIGNAL( toggled(bool) ), this, SLOT( update_names() ) );
	connect( ui->cbx_brackets, SIGNAL( toggled(bool) ), this, SLOT( update_names() ) );
	setAcceptDrops( true );
	
	ui->list_org_names->setModel( &org_name_model );
	ui->list_names->setModel( &name_model );
}

enum under{
	START,
	DOUBLE,
	END
};

QString main_widget::brackets_replace( QString name ){
	std::vector<under> spaces;
	std::vector<int> positions;
	
	//Check if it could start with [
	if( name.startsWith( "_" ) ){
		spaces.push_back( START );
		positions.push_back( 0 );
	}
	
	//Find all places where _[, ]_ and ][ could be
	int pos = 0;
	while( (pos = name.indexOf( "__", pos )) != -1 ){
		spaces.push_back( DOUBLE );
		positions.push_back( pos );
		pos += 2;
	}
	
	//Check if it could end with ]
	if( name.endsWith( "_" ) ){
		spaces.push_back( END );
		positions.push_back( name.size() - 1 );
	}
	
	//Replace on all possibilities
	bool second = false;
	for( unsigned i=0; i<spaces.size(); i++ ){
		switch( spaces[i] ){
			case START: name.replace( positions[i], 1, "[" ); second = true; break;
			case DOUBLE: name.replace( positions[i], 2, (second = !second) ? "_[" : "]_" ); break;
			case END: {
					name.replace( positions[i], 1, "]" );
					
					//If the previous was ]_, change to ][
					if( i && !second ){
						name.replace( positions[i-1]+1, 1, "[" );
						second = true;
					}
				}break;
		}
	}
	
	return name;
}

QString main_widget::rename( QString name ) const{
	if( ui->cbx_spaces->isChecked() )
		name = name.replace( " ", "_", Qt::CaseSensitive );
	
	if( ui->cbx_brackets->isChecked() )
		name = brackets_replace( name );
	
	if( ui->cbx_underscore->isChecked() )
		name = name.replace( "_", " ", Qt::CaseSensitive );
	
	if( ui->cbx_plus->isChecked() )
		name = name.replace( "+", " ", Qt::CaseSensitive );
	
	if( ui->cbx_percent->isChecked() ){
		//Convert %XX stuff
		QRegExp hex( "%[0-9ABCDEF][0-9ABCDEF]" );
		int pos = -1;
		while( ( pos = name.indexOf( hex ) ) != -1 ){
			QString hex;
			hex += name[pos+1];
			hex += name[pos+2];
			
			name.replace( pos, 3, QChar( hex.toInt( 0, 16 ) ) );
		}
	}
	
	return name;
}


void main_widget::clear(){
	org_names.clear();
	names.clear();
	paths.clear();
	
	ui->list_org_names->reset();
	ui->list_names->reset();
}

void main_widget::update_names(){
	names.clear();
	for( int i=0; i<org_names.count(); i++ ){
		names.append( rename( org_names[i] ) );
	}
	
	ui->list_names->reset();
}


void main_widget::rename_files(){
	for( int i=0; i<org_names.count(); i++ ){
		//Don't add '.' if there is no extension
		QString extension = ext.at(i);
		if( !extension.isEmpty() )
			extension = "." + extension;
		QFile::rename( paths.at(i) + org_names.at(i) + extension, paths.at(i) + names.at(i) + extension );
	}
	clear();
}


void main_widget::dragEnterEvent( QDragEnterEvent *event ){
	if( event->mimeData()->hasUrls() )
		event->acceptProposedAction();
}
void main_widget::dropEvent( QDropEvent *event ){
	if( event->mimeData()->hasUrls() ){
		event->setDropAction( Qt::CopyAction );
		
		
		foreach( QUrl url, event->mimeData()->urls() ){
			QFileInfo path( url.toLocalFile() );
			QString name = path.baseName();
			org_names.append( name );
			names.append( rename( name ) );
			ext.append( path.completeSuffix() );
			
			paths.append( path.canonicalPath() + "/" );
		}
		
		ui->list_org_names->reset();
		ui->list_names->reset();
		
		event->accept();
	}
}

