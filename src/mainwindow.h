/*
	This file is part of WebFileFixer.

	WebFileFixer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WebFileFixer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WebFileFixer.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAIN_WIDGET_H
#define MAIN_WIDGET_H

#include <QMainWindow>
#include <QString>
#include <QStringList>
#include "qstring_model.h"



class main_widget: public QMainWindow{
	Q_OBJECT
	
	private:
		class Ui_main_widget *ui;
		
		QStringList paths;
		QStringList org_names;
		QStringList names;
		QStringList ext;
		qstring_model org_name_model;
		qstring_model name_model;
	
	public:
		explicit main_widget();
	
	protected:
		void dragEnterEvent( QDragEnterEvent *event );
		void dropEvent( QDropEvent *event );
	
	private:
		QString rename( QString name ) const;
		static QString brackets_replace( QString name );
	
	private slots:
		void rename_files();
		void clear();
		void update_names();
};

#endif