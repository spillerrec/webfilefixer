/*
	This file is part of WebFileFixer.

	WebFileFixer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WebFileFixer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WebFileFixer.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QSTRING_MODEL_H
#define QSTRING_MODEL_H

#include <QStringList>
#include <QAbstractListModel>


class qstring_model: public QAbstractListModel{
	
	private:
		QStringList *list;
	
	public:
		explicit qstring_model( QStringList *list_to_model ){
			list = list_to_model;
		}
	
	public:
		int rowCount( const QModelIndex &parent = QModelIndex() ) const{
			if( list && !parent.isValid() )
				return list->count();
			else
				return 0;
		}
		QVariant	data( const QModelIndex &index, int role = Qt::DisplayRole ) const{
			if( role == Qt::DisplayRole && index.row() < rowCount() && index.row() >= 0 ){
				return list->at( index.row() );
			}
			
			return QVariant();
		}
};


#endif
